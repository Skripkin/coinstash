export type ComponentSize = 'xs' | 'sm' | 'md' | 'lg' | 'xl'

export type ComponentType = 'filled' | 'outlined'
