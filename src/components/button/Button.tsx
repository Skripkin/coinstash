import React from 'react'

import { ComponentSize, ComponentType } from '@/models'
import Loader from '@/components/loader'

import { getColorClass, getTextColorClass, getSizeClass, getTextSizeClass } from './utils'

export interface ButtonProps extends React.HTMLProps<HTMLButtonElement> {
  text?: string;
  type?: ComponentType
  componentSize?: ComponentSize
  color?: string
  loading?: boolean
  disabled?: boolean
  rounded?: boolean
  icon?: JSX.Element
  iconPosition?: 'left' | 'right'
  classes: string
  textClasses: string
}

const Button: React.FC<ButtonProps> = ({
  text,
  componentSize = 'md',
  type = 'filled',
  color = 'purple',
  disabled = false,
  loading = false,
  icon,
  iconPosition = 'left',
  rounded = false,
  classes,
  textClasses,
  ...props
}) => {
  const sizeClasses = getSizeClass(componentSize)
  const textSizeClasses = getTextSizeClass(componentSize)
  const colorClasses = getColorClass(type, color)
  const textColorClasses = getTextColorClass(type, color)
  const disabledClasses = disabled ? 'opacity-50 pointer-events-none' : ''
  const loadingClasses = loading ? 'cursor-wait' : 'cursor-pointer'
  const roundedClasses = rounded ? 'rounded' : ''

  return (
    <button
      className={`flex items-center gap-[8px] transition duration-300 ease-in-out ${roundedClasses} ${loadingClasses} ${sizeClasses} ${colorClasses} ${disabledClasses} ${classes}`}
      disabled={disabled || loading}
      {...props}
    >
      {iconPosition === 'left' && !loading && icon}
      {loading && <Loader componentSize={componentSize} color={color} />}
      {text ? <span className={`${textSizeClasses} ${textColorClasses} ${textClasses}`}>{text}</span> : null}
      {iconPosition === 'right' && icon}
    </button>
  )
}

export default Button
