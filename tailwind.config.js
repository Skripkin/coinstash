const colors = {
  blue: {
    50: '#F0F4FF',
    100: '#D9E2FC',
    200: '#B7C6FA',
    300: '#95AAFA',
    400: '#738EF9',
    500: '#5C5BD5',
    600: '#4F4EDF',
    700: '#3D3FBF',
    800: '#2E2E94',
    900: '#1F1F69',
  },

  purple: {
    50: '#F6F5FF',
    100: '#EDEBFF',
    200: '#D4CAFF',
    300: '#BBA9FF',
    400: '#9D86FF',
    500: '#8366FF',
    600: '#6A50F8',
    700: '#563DE3',
    800: '#412CBD',
    900: '#2B1D97',
  },

  green: {
    50: '#F0FFF4',
    100: '#C6F6D5',
    200: '#9AE6B4',
    300: '#68D391',
    400: '#48BB78',
    500: '#00CC66',
    600: '#00B359',
    700: '#009E4A',
    800: '#007B36',
    900: '#00582C',
  },
};

module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: colors,
    },
  },
  plugins: [],
};
