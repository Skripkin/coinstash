import { ComponentSize } from '@/models'

export const getSizeClass = (size: ComponentSize) => {
  switch (size) {
    case 'xs':
      return 'w-3 h-3'
    case 'sm':
      return 'w-4 h-4'
    case 'md':
      return 'w-5 h-5'
    case 'lg':
      return 'w-6 h-6'
    case 'xl':
      return 'w-7 h-7'
    default:
      return 'w-8 h-8'
  }
}

export const getColorClass = (color: string) => `fill-${color}-600`
