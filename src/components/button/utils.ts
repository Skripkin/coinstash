import { ComponentSize, ComponentType } from '@/models'

export const getSizeClass = (size: ComponentSize) => {
  switch (size) {
    case 'xs':
      return 'py-1 px-2'
    case 'sm':
      return 'py-2 px-4'
    case 'md':
      return 'py-3 px-6'
    case 'lg':
      return 'py-4 px-8'
    case 'xl':
      return 'py-5 px-10'
    default:
      return 'py-3 px-6'
  }
}

export const getTextSizeClass = (size: ComponentSize) => {
  switch (size) {
    case 'xs':
      return 'text-xs'
    case 'sm':
      return 'text-sm'
    case 'md':
      return 'text-base'
    case 'lg':
      return 'text-lg'
    case 'xl':
      return 'text-xl'
    default:
      return 'text-base'
  }
}

export const getColorClass = (type: ComponentType, color: string) => {
  switch (type) {
    case 'filled':
      return `bg-${color}-500 hover:bg-${color}-600`
    case 'outlined':
      return `border border-${color}-500 hover:bg-${color}-600`
  }
}

export const getTextColorClass = (type: ComponentType, color: string) => {
  switch (type) {
    case 'filled':
      return 'text-white'
    case 'outlined':
      return `text-${color} hover:text-white`
  }
}
